# Overview of Olympiads School - Computer Contest - Level 3

<!--- I don't know why vs code formats markdown like this --->
| Main Topics         |                                                                                                                                                                                                                                                     |
| ------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Graph Theory        | Introduction to Graphs<br/>BFS/DFS<br/>Topological Sort <br/>Minimum Spanning Tree (**MST**)<br/>- Prim's MST<br/>- Kruskal's MST<br/>Disjoint Sets<br/>Shortest Path Algorithms<br/>- Dijkstra<br/>- Bellman-Ford<br/>- SPFA <br/>- Floyd Warshall |
| Dynamic Programming | Methods<br/>Knapsack Problem <br/>Sequence-Based DP<br/>Lowest Common/Increasing Subsequence (**LCS/LIS**)<br/>Interval DP<br/>Bitmasks<br/>DP on Trees and Graphs                                                                                  |

Some of the notes will be based off of the olympiads notes.
